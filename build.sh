#!bin/bash

g++ -c -fPIC solve.cpp -o solve.o -std=c++11
g++ -shared -Wl,-soname,solve.so -o solve.so solve.o
rm solve.o
