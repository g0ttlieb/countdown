from flask import Flask, request, render_template
import ctypes
import time

# Initialisation
app = Flask(__name__)

# Main page
@app.route("/")
def main():
    return render_template("main.html")

# Calculate
@app.route("/calculate", methods=['POST'])
def calculate():

    # Extract params
    target = int(request.form["target"])
    nums = [
        int(request.form["num1"]),
        int(request.form["num2"]),
        int(request.form["num3"]),
        int(request.form["num4"]),
        int(request.form["num5"]),
        int(request.form["num6"])]
    nums.sort(reverse=True)

    start = time.time()

    # Find equation with closest value
    eq = postToInfix(solve(target, nums))
    best = int(eval(eq))

    print(time.time() - start)

    # Return
    if(best == target):
        return "It is possible with equation: " + eq
    else:
        return "The closest you could get is " + str(best) + " with an equation: " + eq

# Return: best equation
# Pump out to c++ function
def solve(target, nums):

    # Load lib
    lib = ctypes.cdll.LoadLibrary("./solve.so")

    # Set return type
    lib.solve.restype = ctypes.c_char_p

    # Convert nums list to appropriate
    params = (ctypes.c_int * len(nums))()
    params[:] = nums

    # Pass to cpp and return answer
    return lib.solve(target, params).decode("utf-8")

# Turn postfix notation into infix to display final eq
def postToInfix(postfix):
    l = list()
    infix = ""
    for x in postfix.split():
        if x not in ['+', '-', '/', '*']:
            l.append(x)
        elif l:
            a = l.pop()
            b = l.pop()
            temp = b + x + a
            if x == "+" or x == "-":
                temp = "(" + temp + ")"
            l.append(temp)
    return l.pop()

# Run app
if __name__ == '__main__':
    app.run()
