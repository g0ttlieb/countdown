// Include required libs
#include <iostream>
#include <cstring>

// Constants to prevent Magic Numbers
#define POOL_SIZE 6
#define POOL_CONTENT_SIZE 4
#define MAX_OPTIONS_SIZE 10
#define LARGE_ARRAY_SIZE 100

// Namespace
using namespace std;

// Declare struct
struct returnPair{
    int val = 0;
    char eq[LARGE_ARRAY_SIZE] = "";
};

// Declare functions
returnPair recurseSolve(int target, char fullEq[], char eq[], char pool[POOL_SIZE][POOL_CONTENT_SIZE], int poolSize, int nums, int ops);
void simplifyPostfix(char eq[]);

// Interface with python
extern "C" char const* solve(int target, int nums[]){

    // Convert to strings and place in array 
    char pool[POOL_SIZE][POOL_CONTENT_SIZE];
    for(int i = 0; i < POOL_SIZE; i++)
        sprintf(pool[i], "%d", nums[i]);

    // Extract returned values
    char fullEq[LARGE_ARRAY_SIZE] = "";
    char eq[LARGE_ARRAY_SIZE] = "";
    returnPair result = recurseSolve(target, fullEq, eq, pool, POOL_SIZE, 0, 0);

    // Return
    char const* output = (const char*) result.eq; 
    return output;

}

// Recursivly solve
returnPair recurseSolve(int target, char fullEq[], char eq[], char pool[POOL_SIZE][POOL_CONTENT_SIZE], int poolSize, int nums, int ops){

    // Initialise returnPair struct to store best
    returnPair best;

    // If finished: return (only works for recursed steps)
    if((nums == 1 && ops == 0) && (atoi(eq) == target || poolSize == 0)){
        strcpy(best.eq, fullEq);
        best.val = atoi(eq);
        return best;
    }

    // Initialise
    returnPair newBest;
    int optionsSize = poolSize;
    char options[MAX_OPTIONS_SIZE][POOL_CONTENT_SIZE];
    char newPool[POOL_SIZE][POOL_CONTENT_SIZE];
    for(int i = 0; i < poolSize; i++){
        strcpy(options[i], pool[i]);
    }

    // In this case: can be both num and op so add ops to options
    if(nums-ops >= 2){
        strcpy(options[poolSize], "+");
        strcpy(options[poolSize + 1], "-");
        strcpy(options[poolSize + 2], "*");
        strcpy(options[poolSize + 3], "/");
        optionsSize += 4;
    }

    // Loop over options
    // Current = options[i]
    for(int i = 0; i < optionsSize; i++){
            
        // Only process unique options (works because sorted)
        if(i != 0 && strcmp(options[i], options[i-1]) == 0) continue;

        // Prevent double divide (duplicate of 1 multiple and 1 divide)
        if((strcmp(options[i], "/") == 0) && (fullEq[strlen(fullEq) - 1] == '/')) continue;

        // Create new eq
        char newEq[LARGE_ARRAY_SIZE];
        strcpy(newEq, eq);
        strcat(newEq, " ");
        strcat(newEq, options[i]);

        // Update nums + ops
        int newNums = nums;
        int newOps = ops;
        if(i < poolSize)
            newNums++;
        else
            newOps++;

        // If is legit postfix: simplify
        if((newNums - newOps == 1) && (newNums != 1)){
            simplifyPostfix(newEq);
            // Undesired path: skip
            if(strcmp(newEq, "-1") == 0) continue;
            // Reset nums + ops count
            newNums = 1;
            newOps = 0;
        }

        // Create new full eq
        char newFull[LARGE_ARRAY_SIZE];
        strcpy(newFull, fullEq);
        strcat(newFull, " ");
        strcat(newFull, options[i]);

        // Create new pool - remove used entry
        bool removed = false;
        int pos = 0;
        for(int j = 0; j < poolSize; j++){
            if(!removed && strcmp(pool[j], options[i]) == 0)
                removed = true;
            else
                strcpy(newPool[pos++], pool[j]);
        }
        // Decrease if something was removed (i.e. current not an op)
        if(removed) poolSize--;

        // Recurse
        newBest = recurseSolve(target, newFull, newEq, newPool, poolSize, newNums, newOps);

        // Restore poolSize variable if was decreased before
        if(removed) poolSize++;

        // Update best
        if(newBest.val == target){
            strcpy(best.eq, newBest.eq);
            best.val = newBest.val;
            return best;
        }
        if(abs(newBest.val - target) < abs(best.val - target)){
            strcpy(best.eq, newBest.eq);
            best.val = newBest.val;
        }

    }

    // Returns best of children when no perfect match found
    return best;
}

// Simplifies postfix equation
// If undesired path: return "-1"
void simplifyPostfix(char eq[]){

    // Initialise
    int stk[POOL_SIZE];
    int i = 0;
    char tmp[20] = "";
    int tmpPos = 0;

    // Loop over equation
    for(int pos = 0; pos < strlen(eq); pos++){

        // Current character
        char current = eq[pos];

        // If space: store num in tmp and reset tmp
        if(current == ' '){

            // If nothing in array: skip
            if(strcmp(tmp, "") == 0) continue;

            // Complete char array, store and reset
            tmp[tmpPos] = '\0';
            stk[i++] = atoi(tmp);
            tmp[0] = '\0';
            tmpPos = 0;

        }
        // If greater than '/' is not operators - digit
        // Add numbers to array to be stored once finished
        else if(current > '/')
            tmp[tmpPos++] = current;
        // Pop 2 prev nums and calculate new based on operator
        // Includes validity checking - set to "-1"  and return if undesired
        else{
            int fnl;
            int b = stk[--i];
            int a = stk[--i];
            if(current == '+'){
                if(a<b){
                    strcpy(eq, "-1");
                    return;
                }
                fnl = a+b;
            }
            else if(current == '-'){
                if(a<=b) {
                    strcpy(eq, "-1");
                    return;
                }
                fnl = a-b;
            }
            else if(current == '*'){
                if(a<b || a==1 || b==1){
                    strcpy(eq, "-1");
                    return;
                }
                fnl = a*b;
            }
            else if(current == '/'){
                if(a==0 || b <= 1 || a%b){
                    strcpy(eq, "-1");
                    return;
                }
                fnl = a/b;
            }
            stk[i++] = fnl;
        }
    }

    // Set string to be final val
    sprintf(eq, "%d", stk[0]);

}
