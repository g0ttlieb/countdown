# Countdown

Solves maths equations as per the popular gameshow countdown.

It uses C++ to do the brute forcing and python (Flask) to run the front end. ctypes is used to
interface between the 2. As some point, in the name of speed, most of the C++ ended up back at plain old C!

NOTE: build.sh is used to rebuild the c++ component whenever it is changed

Hosted at: [countdown-solver.herokuapp.com](https://countdown-solver.herokuapp.com/)

## TODO
1. Style improvements
2. Further speed improvements

## A Note from 2019
Wow some of that C++ code is ugly! The `#define`s are showing their age and there are way too many redundant comments! Oh well ...
