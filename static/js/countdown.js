var button = document.getElementsByTagName("button")[0];
button.addEventListener("click", calculate);

function calculate(event){
    
    // Get output field
    var output = document.getElementById("response");

    // Check all valid
    var numbers = document.getElementsByTagName("input");
    for(i = 0; i < 7; i++){
        if(numbers[i].value < 1 || numbers[i].value > 999){
            output.textContent = "Invalid entry. Ensure all numbers are between 1 and 999!";
            return;
        }
    }

    // Create param string
    var param = "target=" + numbers[0].value;
    for(i = 1; i < 7; i++){
        param += "&num" + i + "=" + numbers[i].value;
    }
    output.textContent = param;
    
    // AJAX server request
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        output.textContent = "Calculating ... ";
        // Valid response
        if(this.readyState==4 && this.status==200)
            output.textContent = this.responseText;
        // Timeout error
        else if(this.status==503)
            output.textContent = "Timeout Error. There might be a possible solution but ... it couldn't be found in time! Sorry!";
    }
    xhttp.open("POST", "/calculate", true);
    xhttp.setRequestHeader('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
    xhttp.send(param);

}
